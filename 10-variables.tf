
variable "envtype"          {}
variable "envname"          {}
variable "domain"           {}
variable "bootstrap_bucket" {}

variable "linux_source_image" { default = "" }
variable "windows_source_image" { default = "" }

variable "linux_asg_min" { default = "1" }
variable "linux_asg_max" { default = "1" }

variable "windows_enabled" {
  default = "0"
}

variable "windows_asg_min" { default = "0" }
variable "windows_asg_max" { default = "0" }
