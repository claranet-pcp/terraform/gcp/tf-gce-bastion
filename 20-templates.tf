
resource "google_compute_instance_template" "linux_bastion_template" {
  name_prefix = "bastion"

  machine_type   = "g1-small"
  can_ip_forward = true

  scheduling {
    automatic_restart    = true
    on_host_maintenance  = "MIGRATE"
  }

  disk {
    device_name  = "sda"
    source_image = "${var.linux_source_image}"
    boot         = true
  }

  network_interface {
    network = "default"

    access_config {

    }
  }

  metadata {
    envname            = "${var.envname}"
    domain             = "${var.domain}"
    startup-script-url = "gs://${var.bootstrap_bucket}/bastion-linux-script.sh"
  }

  service_account {
    scopes = ["storage-ro","compute-rw"]
  }
}

resource "google_storage_bucket_object" "linux_bastion_script" {
  name   = "bastion-linux-script.sh"
  bucket = "${var.bootstrap_bucket}"
  source = "${path.module}/include/bastion-linux-script.sh"
}


resource "google_compute_instance_template" "windows_bastion_template" {
  name_prefix    = "bastion-win"
  machine_type   = "g1-small"
  can_ip_forward = true

  scheduling {
    automatic_restart    = true
    on_host_maintenance  = "MIGRATE"
  }

  disk {
    device_name  = "sda"
    source_image = "${var.windows_source_image}"
    boot         = true
  }

  network_interface {
    network = "default"

    access_config {

    }
  }

  metadata {
    envname            = "${var.envname}"
    domain             = "${var.domain}"
  }

  service_account {
    scopes = ["storage-ro","compute-rw"]
  }
}
