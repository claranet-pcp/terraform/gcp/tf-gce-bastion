
module "bastion-linux-asg" {
  source            = "../tf-gce-asg"
  name              = "bastion"
  instance_template = "${google_compute_instance_template.linux_bastion_template.self_link}"
  asg_min           = "${var.linux_asg_min}"
  asg_max           = "${var.linux_asg_max}"
}

module "bastion-windows-asg" {
  source            = "../tf-gce-asg"
  enabled           = "${var.windows_enabled}"
  name              = "bastion-win"
  instance_template = "${google_compute_instance_template.windows_bastion_template.self_link}"
  asg_min           = "${var.windows_asg_min}"
  asg_max           = "${var.windows_asg_max}"
}
