#!/bin/bash -x

# == Puppet Facts
mkdir -p /etc/facter/facts.d
envname=$(curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/attributes/envname)
domain=$(curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/attributes/domain)
zone=$(curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/zone | cut -f 4 -d '/')
ip=$(curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/ip)
natip=$(gcloud compute addresses list | grep ${zone} | awk '{ print $3 }')

echo envname=${envname} > /etc/facter/facts.d/gce.txt
echo profile=${profile} >> /etc/facter/facts.d/gce.txt
echo domain=${domain} >> /etc/facter/facts.d/gce.txt
echo zone=${zone} >> /etc/facter/facts.d/gce.txt
echo ip=${ip} >> /etc/facter/facts.d/gce.txt
echo natip=${natip} >> /etc/facter/facts.d/gce.txt

cat /etc/facter/facts.d/gce.txt

# == Puppet Repo
echo "Prepping custom puppet repo"
cat <<HERE > /etc/yum.repos.d/custom-puppet.repo
[custom-puppet]
name=Custom Puppet Repository
enabled=1
baseurl=file:///opt/repos/repos/puppet
gpgcheck=0
metadata_expire=1m
HERE

# == Install code and run Puppet
yum install -y ${envname}-puppet
puppet-masterless-mco
